<?php
namespace frontend\controllers;

use yii\web\Controller;

/**
 * Generator controller
 */
class GeneratorController extends Controller
{
    /**
     * Generates 10000 random posts.
     *
     */
    public function actionIndex()
    {
        $connection = new \yii\db\Query();

        $authors = $connection->select('id')->from('authors')->all();
        $languages = $connection->select('*')->from('languages')->all();

        //Start point of our date range.
        $start = strtotime("01 January 2017");
        //End point of our date range.
        $end = strtotime("08 August 2017");

        for($i = 0; $i < 10000; $i++) {
            $randomAuthor = $authors[array_rand($authors)];
            $randomLanguage = $languages[array_rand($languages)];
            $connection->createCommand()->insert('posts', [
                'author_id' => $randomAuthor['id'],
                'language_id' => $randomLanguage['id'],
                'publication_date' => $this->randomdate($start, $end),
                'title' => $this->randomTitle($randomLanguage['id']),
                'description' => $this->randomDescription($randomLanguage['id']),
                'likes' => $this->randomLikesByAlgorithm($i),
            ])->execute();
        }

        return('generated');
    }


    /**
     * Generates and returns random date between 01.01.2017 - 08.08.2017.
     *
     * @return date
     */
    public function randomdate($start, $end)
    {
        //Custom range.
        $timestamp = mt_rand($start, $end);

        //Print it out.
        return date("Y-m-d", $timestamp);
    }

    /**
     * Generates and returns a random title consisting of 4-6 words already defined in wordsForTitle param.
     *
     * @return string
     */
    public function randomTitle($language)
    {
        $words = \Yii::$app->params['wordsForTitle'][$language];
        $randomWordsKeys = array_rand($words, rand(4,6));
        foreach ($randomWordsKeys as $k => $v) {
            $title[] = $words[$v];
        }

        return $this->mb_ucfirst(implode(' ', $title));
    }

    /**
     * Generates and returns a random text consisting of 3-4 sentences, which consist of 5-8 words already defined in wordsForDescription param.
     *
     * @return string
     */
    public function randomDescription($language)
    {
        $countOfSentences = rand(3,4);
        $words = \Yii::$app->params['wordsForDescription'][$language];
        for ($i = 1; $i<=$countOfSentences; $i++){
            $randomWordsKeys[$i] = array_rand($words, rand(5,8));
            foreach ($randomWordsKeys[$i] as $k => $v) {
                $sentence[$i][] = $words[$v];
            }
            $sentences[] = $this->mb_ucfirst(implode(' ', $sentence[$i]));
        }

        return implode('. ', $sentences).'.';
    }

    /**
     * Generates and returns a random amount of likes from 1 to 100.
     *
     * @return int
     */
    public function randomLikesByAlgorithm($i)
    {
        // the algorithm will return:
        //* in 20% cases - 71 to 100 likes
        //* in 30% cases - 31 to 70 likes
        //* in 50% cases - 1 to 30 likes
        // the idea of the algorithm is when dividing the $i keys of the FOR loop above on 10,
        // the remainder correspondingly is:
        //* in 20% cases - 0 or 1
        //* in 30% cases - 7, 8 or 9
        //* in 50% cases - 2, 3, 4, 5, 6

        if(in_array($i%10, [0, 1]))
            return rand(71, 100);
        elseif (in_array($i%10, [2, 3, 4, 5, 6]))
            return rand(1, 30);
        else
            return rand(31, 70);
    }

    /**
     * Capitalizing the first character of stings in UTF-8 format.
     *
     * @return string
     */
    function mb_ucfirst($string, $e ='utf-8') {
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
            $string = mb_strtolower($string, $e);
            $upper = mb_strtoupper($string, $e);
            preg_match('#(.)#us', $upper, $matches);
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
        } else {
            $string = ucfirst($string);
        }
        return $string;
    }
}

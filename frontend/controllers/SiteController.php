<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    /**
     * Generates 10000 random posts.
     *
     */
    public function actionGenerator()
    {

        $connection = new \yii\db\Query();
        $authors = $connection->select('id')->from('authors')->all();
        $languages = $connection->select('*')->from('languages')->all();
        for($i = 0; $i < 10000; $i++) {
            $randomAuthor = $authors[array_rand($authors)];
            $randomLanguage = $languages[array_rand($languages)];
            $connection->createCommand()->insert('posts', [
                'author_id' => $randomAuthor['id'],
                'language_id' => $randomLanguage['id'],
                'publication_date' => $this->actionRandomdate(),
                'title' => $this->actionRandomTitle($randomLanguage['id']),
                'description' => $this->actionRandomDescription($randomLanguage['id']),
                'likes' => $this->actionRandomLikesByAlgorithm($i),
            ])->execute();
        }

        die('done');
    }


    /**
     * Generates and returns random date between 01.01.2017 - 08.08.2017.
     *
     * @return date
     */
    public function actionRandomdate()
    {
        //Start point of our date range.
        $start = strtotime("01 January 2017");

        //End point of our date range.
        $end = strtotime("08 August 2017");

        //Custom range.
        $timestamp = mt_rand($start, $end);

        //Print it out.
        return date("Y-m-d", $timestamp);
    }

    /**
     * Generates and returns a random title consisting of 4-6 words already defined in wordsForTitle param.
     *
     * @return string
     */
    public function actionRandomTitle($language)
    {
        $words = \Yii::$app->params['wordsForTitle'][$language];
        $randomWordsKeys = array_rand($words, rand(4,6));
        foreach ($randomWordsKeys as $k => $v) {
            $title[] = $words[$v];
        }

        return $this->mb_ucfirst(implode(' ', $title));
    }

    /**
     * Generates and returns a random text consisting of 3-4 sentences, which consist of 5-8 words already defined in wordsForDescription param.
     *
     * @return string
     */
    public function actionRandomDescription($language)
    {
        $countOfSentences = rand(3,4);
        $words = \Yii::$app->params['wordsForDescription'][$language];
        for ($i = 1; $i<=$countOfSentences; $i++){
            $randomWordsKeys[$i] = array_rand($words, rand(5,8));
            foreach ($randomWordsKeys[$i] as $k => $v) {
                $sentence[$i][] = $words[$v];
            }
            $sentences[] = $this->mb_ucfirst(implode(' ', $sentence[$i]));
        }

        return implode('. ', $sentences).'.';
    }

    /**
     * Generates and returns a random amount of likes from 1 to 100.
     *
     * @return int
     */
    public function actionRandomLikesByAlgorithm($i)
    {
        // the algorithm will return:
        //* in 20% cases - 71 to 100 likes
        //* in 30% cases - 31 to 70 likes
        //* in 50% cases - 1 to 30 likes
        // the idea of the algorithm is when dividing the $i keys of the FOR loop above on 10,
        // the remainder correspondingly is:
        //* in 20% cases - 0 or 1
        //* in 30% cases - 7, 8 or 9
        //* in 50% cases - 2, 3, 4, 5, 6

        if(in_array($i/10, [0, 1]))
            return rand(71, 100);
        elseif (in_array($i%10, [2, 3, 4, 5, 6]))
            return rand(1, 30);
        else
            return rand(31, 70);
    }

    /**
     * Capitalizing the first character of stings in UTF-8 format.
     *
     * @return string
     */
    function mb_ucfirst($string, $e ='utf-8') {
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
            $string = mb_strtolower($string, $e);
            $upper = mb_strtoupper($string, $e);
            preg_match('#(.)#us', $upper, $matches);
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
        } else {
            $string = ucfirst($string);
        }
        return $string;
    }
}
